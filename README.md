
# README #

This Repository Contains Support Files needed to Set an EC2 Instance with Galaxy Project

### Requirements ###

* Basic Knowledge of Ansible Scripts
* [Installation Tutorial for Reference](https://galaxyproject.github.io/training-material/topics/admin/tutorials/ansible-galaxy/tutorial.html)

### How do I get set up? ###

* git clone https://bitbucket.org/sshanbh1/galaxy-ec2-setup.git
* Go to the "Galaxy" folder.
* Check if Ansible is Installed by typing 'ansible' in the command prompt. 
* If not, follow the steps : [Installing Ansible on Ubuntu](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#latest-releases-via-apt-ubuntu)
* Copy website certificates to "/etc/nginx/ssl/" folder.
* Edit "group_vars/galaxyservers.yml" change {{ PRIVATE KEY Content }} with the content inside the \*.key certificate file.
* Edit hosts file and replace "usegalaxy.bioviz.org" with the host you want the website to be hosted.
* First Run "ansible-galaxy install -p roles -r requirements.yml"
* This will download all the roles required for the Ansible and Galaxy Setup - [From Ansible Galaxy](https://galaxy.ansible.com/)
* Run "sudo ansible-playbook galaxy.yml"

In the end you should see the output like
```
RUNNING HANDLER [restart galaxy] ****************************************
changed: [galaxy.example.org]
```
If there are any errors contant the Developer or refer [Installation Guide](https://galaxyproject.github.io/training-material/topics/admin/tutorials/ansible-galaxy/tutorial.html)


### Who do I talk to? ###

* Sameer Shanbhag [sshanbh1@uncc.edu](mailto:sshanbh1@uncc.edu)
* Dr. Loraine [Ann.Loraine@uncc.edu](mailto:Ann.Loraine@uncc.edu)
